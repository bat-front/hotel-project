# Hotel Project

As an admin this project will allow you, list rooms by status, list reservations, list client, form for reservation functionality, form for checkin functionality.
Just have fun!

---
## Requirements

For development, you will only need Node.js and a node global package, installed in your environement.

### Node
- #### Node installation on Windows

  Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Node installation on Ubuntu

  You can install nodejs and npm easily with apt install, just run the following commands.

      $ sudo apt install nodejs
      $ sudo apt install npm

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following command.

    $ node --version

    $ npm --version
    

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g

###

---

## Install

    $ git clone https://gitlab.com/bat-front/hotel-project.git
    $ cd hotel-project
    $ npm install


## Running the project

    $ node server.js
    
     or

    $ npm start
    

## Configure app

You will need nothing because is a vanilla js project:

In case you need to see tests and linter:

Run test


> npm run test


Run eslint


> npm run lint:fix

