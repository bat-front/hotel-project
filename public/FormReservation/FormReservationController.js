import FormReservationService from './js/FormReservationService.js';
import FormReservationValidation from './js/FormReservationValidation.js';

let formGuest;
let firstNameGues;
let lastNameGuest;
let numberDNIGuest;
let partnerGuestCheck; 
let firstNamePartner; 
let lastNamePartner; 
let dateCheckinRoom;
let dateCheckoutRoom;
let penthouseRoom;
let matrimonialRoom;
let doubleRoom;
let singleRoom; 
let btnReservation;
let btnRegister;
let btnSubmit;
let errorMesagge;
let form_close;

let formValidator = new FormReservationValidation();
let formService = new FormReservationService();

window.addEventListener('load', function(){
    formGuest = document.getElementById('form');
    firstNameGues = document.getElementById('fName1');
    lastNameGuest = document.getElementById('lName1'); 
    numberDNIGuest = document.getElementById('identity');
    partnerGuestCheck = document.getElementById('partner');
    firstNamePartner = document.getElementById('fName2');
    lastNamePartner = document.getElementById('lName2');

    dateCheckinRoom = document.getElementById('check-in');
    dateCheckoutRoom = document.getElementById('check-out');
    penthouseRoom = document.getElementById('penthouse');
    matrimonialRoom = document.getElementById('matrimonial');
    doubleRoom = document.getElementById('double');
    singleRoom = document.getElementById('single');
    btnReservation = document.getElementById('btn_reservation');
    btnRegister = document.getElementById('btn_registration');
    errorMesagge = document.getElementById('error-message');
    btnSubmit = document.getElementById('btn-submit');
    form_close = document.getElementById('close_form');

    formService.ChargePenthouseRooms(true);
    EventListeners();
});

function EventListeners(){
    
    firstNameGues.addEventListener('blur',()=>{
        formValidator.ValidateNameGuest(firstNameGues);
    } );

    lastNameGuest.addEventListener('blur', () => {
        formValidator.ValidateLastNameGuest(lastNameGuest);
    });
    
    numberDNIGuest.addEventListener('blur', () => {
        formValidator.ValidateDNI(numberDNIGuest);
    });

    partnerGuestCheck.addEventListener('blur', () => {
        formValidator.AblePartner(partnerGuestCheck, firstNamePartner, lastNamePartner);
    });

    firstNamePartner.addEventListener('blur', () =>{
        formValidator.ValidateNamePartner(firstNamePartner);
    });

    lastNamePartner.addEventListener('blur', () => {
        formValidator.ValidateLastNamePartner(lastNamePartner);
    });
   
    dateCheckinRoom.addEventListener('blur', ()=>{
        formValidator.ValidateDateCheckin(dateCheckinRoom, dateCheckoutRoom);
    });

    dateCheckoutRoom.addEventListener('blur', ()=>{
        formValidator.ValidateDateCheckOut(dateCheckoutRoom, dateCheckinRoom);
        if(dateCheckoutRoom.style.borderColor == 'green'){
            formService.CalculatePrice();
        }
    });
    
    penthouseRoom.addEventListener('click', () => {
        formService.ChargePenthouseRooms(penthouseRoom.checked);
    });

    matrimonialRoom.addEventListener('click', () => {
        formService.ChargeMatrimonialRooms(matrimonialRoom.checked);
    });

    doubleRoom.addEventListener('click', () =>{
        formService.ChargeDoubleRooms(doubleRoom.checked);
    });

    singleRoom.addEventListener('click', () => {
        formService.ChargeSingleRooms(singleRoom.checked);
    });
    
    formGuest.addEventListener('submit', (e) =>{
        e.preventDefault();
        let isValidForm = CorrectForm();
        if(isValidForm == true){
            if( btnSubmit.innerText == 'MAKE RESERVATION' ){
                formService.MakeReservationService();
            }else{
                formService.MakeCheckInService();
            }
            ResetStyleForm();
            formGuest.reset()    
            document.getElementById('Home').style.filter = 'none';
            document.getElementById('modal').style.display = 'none';        

        }else{
            errorMesagge.style.display = 'block';
        }
    });

    formGuest.addEventListener('reset', () => {
        formService.ChargePenthouseRooms(true);
    });


    btnReservation.addEventListener('click', () => {
        document.getElementById('Home').style.filter = 'blur(10px)';
        document.getElementById('modal').style.display = 'block';
        btnSubmit.innerText = 'MAKE RESERVATION';
    });

    btnRegister.addEventListener('click', () => {
        document.getElementById('Home').style.filter = 'blur(10px)';
        document.getElementById('modal').style.display = 'block';
        btnSubmit.innerText = 'CHECK IN';
    });

    form_close.addEventListener('click', () => {
        document.getElementById('Home').style.filter = 'none';
        document.getElementById('modal').style.display = 'none';
    });
}

function CorrectForm(){

    if( firstNameGues.style.borderColor == 'green' && lastNameGuest.style.borderColor == 'green' && numberDNIGuest.style.borderColor == 'green' ){
        if(partnerGuestCheck.checked == true ){
            if(firstNamePartner.style.borderColor == 'green' && lastNamePartner.style.borderColor == 'green' && dateCheckinRoom.style.borderColor == 'green' && dateCheckoutRoom.style.borderColor == 'green'){
                return true;
            }else{
                return false
            }
        }else if (dateCheckinRoom.style.borderColor == 'green' && dateCheckoutRoom.style.borderColor == 'green'){
            return true;
        }else{
            return false;
        }
    }
    else{
        return false;
    }
}

function ResetStyleForm(){
    firstNameGues.style.borderColor = '';
    firstNameGues.style.borderWidth = '';
    firstNameGues.style.borderStyle = '';

    lastNameGuest.style.borderColor = '';
    lastNameGuest.style.borderWidth = '';
    lastNameGuest.style.borderStyle = '';
    
    numberDNIGuest.style.borderColor = '';
    numberDNIGuest.style.borderWidth = '';
    numberDNIGuest.style.borderStyle = '';
    
    firstNamePartner.style.borderColor = '';
    firstNamePartner.style.borderWidth = '';
    firstNamePartner.style.borderStyle = '';
    
    lastNamePartner.style.borderColor = '';
    lastNamePartner.style.borderWidth = '';
    lastNamePartner.style.borderStyle = '';
    
    dateCheckinRoom.style.borderColor = '';
    dateCheckinRoom.style.borderWidth = '';
    dateCheckinRoom.style.borderStyle = '';
    
    dateCheckoutRoom.style.borderColor = '';
    dateCheckoutRoom.style.borderWidth = '';
    dateCheckoutRoom.style.borderStyle = '';
    
    errorMesagge.style.display = 'none';
}
