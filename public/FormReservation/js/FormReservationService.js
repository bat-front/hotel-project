import { HotelClass } from '../../db/classes/HotelClass.js';
import { PersonClass } from '../../db/classes/PersonClass.js';
import { PartnerClass } from '../../db/classes/PartnerClass.js';

export default class FormReservationService{

    MakeReservationService(){
        this.MakeFormAction('Reserved');    
    }

    MakeCheckInService(){
        this.MakeFormAction('Occupied');
    }

    MakeFormAction(action){
        let firstNameGuest = document.getElementById('fName1');
        let lastNameGuest = document.getElementById('lName1');
        let numberDNIGuest = document.getElementById('identity');
        let companion = document.getElementById('partner');
        let firstNamePartner = document.getElementById('fName2');
        let lastNamePartner = document.getElementById('lName2');
    
        let dateCheckinRoom = document.getElementById('check-in');
        let dateCheckoutRoom = document.getElementById('check-out');
        let penthouseRoom = document.getElementById('penthouse');
        let matrimonialRoom = document.getElementById('matrimonial');
        let doubleRoom = document.getElementById('double');
        let singleRoom = document.getElementById('single');
        let numberRoomSelected = document.getElementById('room');
        
        let partner;
        if(companion.checked){
            partner = new PartnerClass(firstNamePartner.value, lastNamePartner.value);
        }
        
        let guest = new PersonClass(firstNameGuest.value, lastNameGuest.value, numberDNIGuest.value, partner);

        let typeRoom = '';
        
        if(penthouseRoom.checked){
            typeRoom = 'Penthouse';
        }else if (matrimonialRoom.checked){
            typeRoom = 'Matrimonial';
        }else if (doubleRoom.checked){
            typeRoom = 'Double';
        }else if (singleRoom.checked){
            typeRoom = 'Single';
        }         
    
        let currentRoom = HotelClass.rooms.find(room => room.Type === typeRoom && room.Id === numberRoomSelected.value && room.RoomStatus === 'Available');
        
        if (currentRoom !== undefined){
            currentRoom.Guest = guest;
            currentRoom.Partners = partner!=null?partner:null;
            currentRoom.CheckIn = dateCheckinRoom.value;
            currentRoom.CheckOut = dateCheckoutRoom.value;
            currentRoom.RoomStatus = action;
            currentRoom.setIdPerson();
            currentRoom.setTotalPrice();
            
            HotelClass.addClient(guest);
            if(partner !== null)HotelClass.addClient(partner);
        }else{
            console.log('No Available room please select other');
        }
    }
    
    ChargeMatrimonialRooms(matrimonialRoom){
        if(matrimonialRoom == true){
            this.ChangeSelectOptions('Matrimonial');
        }
    }
    
    ChargeDoubleRooms(doubleRoom){
        if(doubleRoom == true){
            this.ChangeSelectOptions('Double');
        }
    }
    
    ChargeSingleRooms(singleRoom){
        if(singleRoom == true){    
            this.ChangeSelectOptions('Single');
        }
    }
    
    ChargePenthouseRooms(penthouseRoom){
        if(penthouseRoom == true){
            this.ChangeSelectOptions('Penthouse');
        }
    }
    
    ChangeSelectOptions(typeRooom){
        let numberRoomSelected = document.getElementById('room');
        numberRoomSelected.options.length = 0;
        let changeRooms = HotelClass.rooms.filter(room => room.Type === typeRooom && room.RoomStatus === 'Available');
        let price = 0;
        if(changeRooms !== undefined){
            changeRooms.forEach(room => {
                let option = document.createElement('option');
                option.value = room.Id;
                option.text = room.Id;
                numberRoomSelected.appendChild(option);
                price = room.Price;
            });
            if(document.getElementById('check-out').style.borderColor == 'green'){
                this.CalculatePrice();
            }else{
                document.querySelector('.form-container__field-price').children[0].innerHTML = price;
            }
            
        }else{
            console.log(`No Available ${typeRooom} rooms please select otherType`);
        }
    }

    CalculatePrice(){
        let dateCheckinRoom = document.getElementById('check-in');
        let dateCheckoutRoom = document.getElementById('check-out');
        let penthouseRoom = document.getElementById('penthouse');
        let matrimonialRoom = document.getElementById('matrimonial');
        let doubleRoom = document.getElementById('double');
        let singleRoom = document.getElementById('single');
        let totalPrice = document.querySelector('.form-container__field-price');

        let days = new Date(dateCheckoutRoom.value).getTime() - new Date(dateCheckinRoom.value).getTime();
        let price = 0;
        days = Math.ceil(days / (1000 * 3600 * 24));
        if(penthouseRoom.checked){
            price = days * HotelClass.rooms.find(room => room.Type === 'Penthouse').Price;
            totalPrice.children[0].innerHTML = price;
        }else if (matrimonialRoom.checked){
            price = days * HotelClass.rooms.find(room => room.Type === 'Matrimonial').Price;
            totalPrice.children[0].innerHTML = price;
        }else if (doubleRoom.checked){
            price = days * HotelClass.rooms.find(room => room.Type === 'Double').Price;
            totalPrice.children[0].innerHTML = price;
        }else if (singleRoom.checked){
            price = days * HotelClass.rooms.find(room => room.Type === 'Single').Price;
            totalPrice.children[0].innerHTML = price;
        }
    }
    
}