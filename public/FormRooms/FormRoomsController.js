import FormRoomService from './js/FormRoomService.js';

let option = '';
let form_room;
let penthouse_room;
let matrimonial_room;
let double_room;
let single_room;
let btn_addRoom;
let modal2;
let Home;
let btn_reservation;
let btn_registration;
let modal;
let close_modal2;
let service = new FormRoomService();
window.addEventListener('load', () => {
    form_room = document.getElementById('form_rooms');
    penthouse_room = document.getElementById('penthouse_room');
    matrimonial_room = document.getElementById('matrimonial_room');
    double_room = document.getElementById('double_room');
    single_room = document.getElementById('single_room');
    btn_addRoom = document.getElementById('btn_addRoom');
    modal2 = document.getElementById('modal2');
    Home = document.getElementById('Home');
    btn_reservation = document.getElementById('btn_reservation');
    btn_registration = document.getElementById('btn_registration');
    modal = document.getElementById('modal');
    close_modal2 = document.getElementById('close_modal2');
    
    

    option = penthouse_room.value;
    EventListeners();
});

function EventListeners(){
    matrimonial_room.addEventListener('blur', () => {
        option = matrimonial_room.value;}
    );
    double_room.addEventListener('blur', () => {
        option = double_room.value;}
    );
    single_room.addEventListener('blur', () => {
        option = single_room.value;}
    );
    form_room.addEventListener('submit', (e) =>{
        e.preventDefault();
        service.AddNewRoom(option);
        service.CloseModal1(modal2,Home);
    });
    btn_addRoom.addEventListener('click', () => {
        service.ShowModal(modal2,Home);
        service.SwitchModal(modal);
    });
    btn_reservation.addEventListener('click', () => {
        service.CloseModal(modal2,Home);
    });
    btn_registration.addEventListener('click', () => {
        service.CloseModal(modal2,Home);
    });
    close_modal2.addEventListener('click', () => {
        service.CloseModalButton(modal2,Home);
    });
}