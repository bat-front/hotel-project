import { HotelClass } from "../../db/classes/HotelClass.js";
import { RoomClass } from '../../db/classes/RoomClass.js';
import RoomService from "../../ListRooms/js/ListRoomsService.js";
var roomService = new RoomService();
export default class FormRoomService{
    AddNewRoom(option){
        let newRoom = new RoomClass(option,null,null,null,'Available');
        HotelClass.addRoom(newRoom);
        roomService.UpdateRoomList(HotelClass.listAllRooms());
    }
    
    ShowModal(modal2,Home){
        modal2.style.display = 'block';
        Home.style.filter = 'blur(10px)';
    }
    CloseModal(modal2){
        modal2.style.display = 'none';
    }
    CloseModalButton(modal2){
        modal2.style.display = 'none';
        Home.style.filter = 'none';
    }
    CloseModal1(modal,Home){
        modal.style.display = 'none';
        Home.style.filter = 'none';
    }
    SwitchModal(modal){
        modal.style.display = 'none';
    }
}