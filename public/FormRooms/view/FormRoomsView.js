export const htmlFormRoom = `
<form id="form_rooms" class="room_form">
          <div class="close_room_form"><p id='close_modal2'>❌</p></div>
        <h3>Room Information</h3>
        <br>
        <br>
        <div class="form-container__radio-btn">
          <label for="penthouse">
            <input type="radio" id="penthouse_room" name="type_room" value="Penthouse" checked>
            <span>Penthouse</span>
          </label>
          <label for="matrimonial">
            <input type="radio" id="matrimonial_room" name="type_room" value="Matrimonial">
            <span>Matrimonial</span>
          </label>
          <label for="double">
            <input type="radio" id="double_room" name="type_room" value="Double">
            <span>Double</span>
          </label>
          <label for="single">
            <input type="radio" id="single_room" name="type_room" value="Single">
            <span>Single</span>
          </label>
        </div>
        <br>
        <button type="submit" id="btn-room-submit" class="btn btn-primary">ADD NEW ROOM</button>
      </form>
`;