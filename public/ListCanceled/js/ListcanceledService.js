import { HotelClass } from '../../db/classes/HotelClass.js';

export class CanceledReservationsService{

    UpdateCanceledReservationList(){
        let room_table = document.getElementById('Canceled_List');
        room_table.innerHTML = '';
        HotelClass.listAllCanceledReservations().forEach(canceledReservation => {
            
            let rId = document.createTextNode(canceledReservation.id);
            let rGuest = document.createTextNode(canceledReservation.firstName + ' ' + canceledReservation.lastName);
            let rDNI = document.createTextNode(canceledReservation.DNI);
            let rPartner = document.createTextNode(canceledReservation.lastNamePartner + ' ' + canceledReservation.firstNamePartner);
            let rRoom = document.createTextNode(canceledReservation.room);
            let rCheckin = document.createTextNode(canceledReservation.checkIn);
            let rCheckout = document.createTextNode(canceledReservation.checkOut);
            let rStatus = document.createTextNode(canceledReservation.status);
            let rPrice = document.createTextNode(canceledReservation.price);
            let rAction = document.createElement('button');
            rAction.innerHTML = '✔';
            let rActions = [rAction];
            let arr = [rId,rGuest,rDNI,rPartner,rRoom,rCheckin,rCheckout,rStatus,rPrice,rActions];
            
            let newRow = room_table.insertRow();
            let newCell;
            arr.forEach(element => {
                newCell = newRow.insertCell();

                if(Array.isArray(element)){
                    newCell.appendChild(element[0]);
                }
                else{
                    newCell.appendChild(element);
                }
            });
        });
    }   
}