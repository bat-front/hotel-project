export const htmlListCanceled = `
<br>
<h3>List of Canceled Reservations</h3>
<br>
<br>
<table>
  <thead>
    <th>ID</th>
    <th>Guest</th>
    <th>DNI</th>
    <th>Partner</th>
    <th>Room</th>
    <th>Check-In</th>
    <th>Check-Out</th>
    <th>Status</th>
    <th>Price</th>
    <th>Actions</th>
  </thead>
  <tbody id="Canceled_List">
  </tbody>
</table>
`;
