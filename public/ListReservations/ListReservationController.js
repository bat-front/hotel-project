import ListReservationService from './js/ListReservationService.js';
import { HotelClass } from '../db/classes/HotelClass.js';

let listReservationService = new ListReservationService();
window.addEventListener('load', () => {
    let list = HotelClass.listAllRooms();
    listReservationService.UpdateReservationList(list);

    const filterForm = document.getElementById('form-filter-reservation');
    const c = filterForm.querySelectorAll('input');
    
    filterForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const filterOpt = [];
        c.forEach((r) => filterOpt.push(r.value));
        listReservationService.UpdateReservationList(listReservationService.ApplyFilters(list, filterOpt));
    })

    let form = document.getElementById('form');
    form.addEventListener('reset', () => {
        listReservationService.UpdateReservationList(list);
    });
});