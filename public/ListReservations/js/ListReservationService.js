export default class ReservationService{
    UpdateReservationList(list){
        let room_table = document.getElementById('Reservation_List');
        room_table.innerHTML = '';
        if (list.length === 0) room_table.innerHTML = '<div class="no-content"><h1>Nothing to show</h1></div>';
        list.forEach(room => {
            let Rid = document.createTextNode(room.Guest ? room.Guest?.Id : '- -');
            let Rguest = document.createTextNode(room.Guest? room.Guest?.FirstName+' '+room.Guest?.LastName : 'No Guest');
            let Rdni = document.createTextNode(room.Guest? room.Guest?.Dni : '- - -');
            let Rpartner = document.createTextNode(room.Guest?.Partners? room.Guest?.Partners?.FirstName+' '+room.Guest?.Partners?.LastName : 'No Partner');
            let Rroom = document.createTextNode(room.Id);
            let Rcheckin = document.createTextNode(room.CheckIn? room.CheckIn : '- - -');
            let Rcheckout = document.createTextNode(room.CheckOut? room.CheckOut : '- - -');
            let Rstatus = document.createTextNode(room.RoomStatus);
            let Rprice = document.createTextNode(`${room.TotalPrice} $`);
            let Ractions1 = document.createElement('button');
            Ractions1.innerHTML = '✔';
            let Ractions2 = document.createElement('button');
            Ractions2.innerHTML = '✘';
            let Ractions3 = document.createElement('button');
            Ractions3.innerHTML = '✎';
            let Ractions = [Ractions1, Ractions2, Ractions3];
            let arr = [Rid,Rguest,Rdni,Rpartner,Rroom,Rcheckin,Rcheckout,Rstatus,Rprice,Ractions];
            if(room.RoomStatus == 'Reserved'){
                let newRow = room_table.insertRow();
                let newCell;
                arr.forEach(element => {
                    newCell = newRow.insertCell();
                    if(Array.isArray(element)){
                        newCell.appendChild(element[0]);
                        newCell.appendChild(element[1]);
                        newCell.appendChild(element[2]);
                    }
                    else{
                        newCell.appendChild(element);
                    }
                
                });
            }
        })
    }

    ApplyFilters (list, [ checkin, checkout ]) {
        if (checkin === '' && checkout === '') return list;
        let filteredList = list;

        if (checkin !== '') {
            filteredList = filteredList.filter((room) => {
                if (room.CheckIn >= checkin) {
                    return room;
                }
            });
        }

        if (checkout !== '') {
            filteredList = filteredList.filter((room) => {
                if (room.CheckIn <= checkout) {
                    return room;
                }
            });
        }
        return filteredList;
    }
}
