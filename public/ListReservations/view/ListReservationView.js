export const htmlListReservations = `<h3>List of reservations</h3>
<br>
<form class="filters" id="form-filter-reservation">
  <div class="filterBy">
    <p>Date from: </p>
    <input id="DateFrom" type="date" name="dateFrom">
  </div>
  <div class="filterBy">
    <p>to: </p>
    <input id="DateTo" type="date" name="dateFrom">
  </div>
  <div class="ApplyFilterButton">
    <button type="submit" id="DateFilter">Apply filter</button>
  </div>
</form>
<br>
<table>
  <thead>
    <th>ID</th>
    <th>Guest</th>
    <th>DNI</th>
    <th>Partner</th>
    <th>Room</th>
    <th>Check-In</th>
    <th>Check-Out</th>
    <th>Status</th>
    <th>Total Price</th>
    <th>Actions</th>
  </thead>
    <tbody id="Reservation_List">
  </tbody>
</table>`;