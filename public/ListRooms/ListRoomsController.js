import ListRoomsService from './js/ListRoomsService.js' ;
import { HotelClass } from '../db/classes/HotelClass.js';

let listRoomsService = new ListRoomsService();
let formReservation;

window.addEventListener('load', function(){
    let list = HotelClass.listAllRooms();
    listRoomsService.UpdateRoomList(list);
    formReservation = document.getElementById('form');

    const filtersForm = document.getElementById('form-filter-room');
    const c = filtersForm.querySelectorAll('select');
    filtersForm.addEventListener('submit', (e) => {
        e.preventDefault();
        const filterOpt = [];
        c.forEach((r) => filterOpt.push(r.value));
        listRoomsService.UpdateRoomList(listRoomsService.ApplyFilters(list, filterOpt));
    })
    EventListeners();
});

function EventListeners(){
    formReservation.addEventListener('reset', function(){
        listRoomsService.UpdateRoomList(HotelClass.listAllRooms());
    });   
}