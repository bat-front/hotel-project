import { HotelClass } from '../../db/classes/HotelClass.js';

export default class RoomService {
    UpdateRoomList(list){
        list = this.SortRooms(list);
        let room_table = document.getElementById('Room_List');
        room_table.innerHTML = '';
        if (list.length === 0) room_table.innerHTML = '<div class="no-content"><h1>Nothing to show</h1></div>';
        list.forEach(room => {
            let Rid = document.createTextNode(room.Id);
            let Rtype = document.createTextNode(room.Type);
            let Rguest = document.createTextNode(room.Guest? room.Guest?.FirstName+' '+room.Guest?.LastName : 'No Guest');
            let Rpartner = document.createTextNode(room.Guest?.Partners? room.Guest?.Partners?.FirstName+' '+room.Guest?.Partners?.LastName : 'No Partner');
            let Rcheckin = document.createTextNode(room.CheckIn? room.CheckIn : '- - -');
            let Rcheckout = document.createTextNode(room.CheckOut? room.CheckOut : '- - -');
            let Rstatus = document.createTextNode(room.RoomStatus);
            let Rprice = document.createTextNode(`${room.Price} $`);
            let Ractions1 = document.createElement('button');
            switch (room.RoomStatus) {
            case 'Available':
                Ractions1.innerHTML = '🔧 to maintenance';
                break;
            case 'Maintenance':
                Ractions1.innerHTML = '☑️ make available';
                break;
            case 'Reserved':
                Ractions1.innerHTML = '🔧 to maintenance';
                Ractions1.disabled = true
                break;
            case 'Occupied':
                Ractions1.innerHTML = '❌ make a checkout';
                break;
            default:
                break;
            }

            let Ractions = [ Ractions1 ];
            let arr = [Rid,Rtype,Rguest,Rpartner,Rcheckin,Rcheckout,Rstatus,Rprice,Ractions];
            let newRow = room_table.insertRow();
            let newCell;
            arr.forEach(element => {
                newCell = newRow.insertCell();
                if(Array.isArray(element)){
                    newCell.appendChild(element[0]);
                } else  {
                    newCell.appendChild(element);
                }
            });
        })

        const listOfActions = room_table.querySelectorAll('tr');

        listOfActions.forEach((row) => {
            row.childNodes[8].addEventListener('click', () => {
                this.ActionButtons(row.childNodes[0].innerHTML, row.childNodes[6].innerHTML)
            })
        })

    }

    SortRooms(list){
        return list.sort((a,b) => {
            if (a.Id > b.Id) return 1;
            if (a.Id < b.Id) return -1;
            return 0;
        });
    }

    ApplyFilters (list, [ type, status ]) {
        if (type === 'no-filter' && status === 'no-filter') return list;
        let filteredList = list;
        if (type !== 'no-filter') {
            filteredList = list.filter((room) => {
                if (room.Type === type) {
                    return room;
                }
            });
        }
        if (status !== 'no-filter') {
            filteredList = filteredList.filter((room) => {
                if (room.RoomStatus === status) {
                    return room;
                }
            });
        }
        return filteredList;
    }

    ActionButtons (id, status) {
        const room = HotelClass.rooms.find(room => room.Id === id && room.RoomStatus === status);
        switch (status) {
        case 'Available':
            room.RoomStatus = 'Maintenance';
            break;
        case 'Maintenance':
            room.RoomStatus = 'Available';
            break;
        case 'Occupied':
            room.RoomStatus = 'Available';
            room.CheckIn = null;
            room.CheckOut = null;
            room.Guest = null;
            break;
        case 'Reserved':
            // room.RoomStatus = 'Available';
            // room.CheckIn = null;
            // room.CheckOut = null;
            // room.Guest = null;
            break;
        default:
            room.RoomStatus = 'Available';
            break;
        }
        this.UpdateRoomList(HotelClass.listAllRooms());
    }
}