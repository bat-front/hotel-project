export const htmlListRoom = `
<h3>List of rooms</h3>
<br>
<form class="filters" id="form-filter-room">
  <div class="filterBy">
    <p>Filter by type: </p>
    <select>
      <option value="no-filter" selected>- - -</option>
      <option value="Single">Single</option>
      <option value="Double">Double</option>
      <option value="Matrimonial">Matrimonial</option>
      <option value="Penthouse">Penthouse</option>
    </select>
  </div>
  <div class="filterBy">
    <p>Filter by status: </p>
    <select>
      <option value="no-filter" selected>- - -</option>
      <option value="Reserved">Reserved</option>
      <option value="Occupied">Occupied</option>
      <option value="Maintenance">Maintenance</option>
      <option value="Available">Available</option>
    </select>
  </div>
  <div class="ApplyFilterButton">
    <button type="submit">Apply filters</button>
  </div>
</form>
<br>
<table>
  <thead>
    <th>ID</th>
    <th>Type</th>
    <th>Guest</th>
    <th>Partner</th>
    <th>Check-In</th>
    <th>Check-Out</th>
    <th>Status</th>
    <th>Price</th>
    <th>Actions</th>
  </thead>
  <tbody id="Room_List">
  </tbody>
</table>
`;