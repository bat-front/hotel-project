import { htmlFormReservation } from '../FormReservation/view/FormReservationView.js';
import { htmlListRoom } from '../ListRooms/view/ListRoomView.js';
import { htmlListReservations } from '../ListReservations/view/ListReservationView.js';
import {htmlListCanceled} from '../ListCanceled/view/ListCanceledView.js';
import { htmlFormRoom } from '../FormRooms/view/FormRoomsView.js';
import { Repository } from '../db/Repository.js';

export function HtmlLoader () {
    window.document.getElementById('ListOfRooms').innerHTML = htmlListRoom;
    window.document.getElementById('ListOfReservations').innerHTML = htmlListReservations;
    window.document.getElementById('form-container').innerHTML = htmlFormReservation;
    window.document.getElementById('ListOfCanceled').innerHTML = htmlListCanceled;
    window.document.getElementById('form-room').innerHTML = htmlFormRoom;
    Repository.fullRepository();
}