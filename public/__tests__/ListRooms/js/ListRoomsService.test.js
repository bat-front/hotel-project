import ListRoomsService from '../../../ListRooms/js/ListRoomsService.js';
import { HotelClass } from '../../../db/classes/HotelClass.js';
import { Repository } from '../../../db/Repository.js';
const service = new ListRoomsService();
Repository.fullRepository();
const list = HotelClass.listAllRooms();

test('the filter does not change the list', () => {
    const result = service.ApplyFilters(list, ['no-filter', 'no-filter']);
    expect(result).toEqual(list);
})

test('it filters just reserved rooms', () => {
    const result = service.ApplyFilters(list, ['no-filter', 'Reserved']);
    const reserved = list.filter((x) => x.RoomStatus === 'Reserved').length;
    expect(result.length).toEqual(reserved);
})

test('it filters just available rooms', () => {
    const result = service.ApplyFilters(list, ['no-filter', 'Available']);
    const reserved = list.filter((x) => x.RoomStatus === 'Available').length;
    expect(result.length).toEqual(reserved);
})
