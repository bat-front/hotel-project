import { PersonClass } from './classes/PersonClass.js';
import { RoomClass } from './classes/RoomClass.js';
import { HotelClass } from './classes/HotelClass.js';
import { PartnerClass } from './classes/PartnerClass.js';
import {CanceledReservation} from './classes/CanceledReservation.js';

export class Repository{
    static fullRepository(){
        var canceledArray = [];
        canceledArray = [
            new CanceledReservation('1041234560', 'Juan', 'Perez', '123456', 'Pablo', 'Perez', '101', '2022-01-01', '2022-01-02', 'Canceled', '100'),
        ];

        var personArray = [];
        personArray = [new PersonClass('Jean','Santivanez','72971718',new PartnerClass('María Fe','Bravo')),
            new PersonClass('Juan','Pérez','74589623',null),
            new PersonClass('José','Jimenez','78952348',null),
            new PersonClass('Aldo','Vargas','45698753',null),
            new PersonClass('Alejandro','Quiñones','75698754',null)];

        var RoomArray = [new RoomClass('Single',null,null,null, 'Available'),
            new RoomClass('Single', null,  null, null, 'Available' ),
            new RoomClass('Single', null,  null, null, 'Available' ),
            new RoomClass('Single', personArray[4],  '2022-05-30', '2022-06-03', 'Reserved' ),
            new RoomClass('Single', null,  null, null, 'Available' ),
            new RoomClass('Single', personArray[2],  '2022-07-25', '2022-07-30', 'Reserved' ),
            new RoomClass('Single', null, null, null, 'Available' ),
            new RoomClass('Double', null, null, null, 'Available' ),
            new RoomClass('Double', null, null, null, 'Available' ),
            new RoomClass('Double', null, null, null, 'Available' ),
            new RoomClass('Double', null, null, null, 'Available' ),
            new RoomClass('Double', null, null, null, 'Available' ),
            new RoomClass('Matrimonial', personArray[0], '2022-07-01', '2022-07-08', 'Reserved' ),
            new RoomClass('Matrimonial', null, null, null, 'Available' ),
            new RoomClass('Matrimonial', personArray[3], '2022-06-20', '2022-06-21', 'Reserved' ),
            new RoomClass('Matrimonial', null, null, null, 'Available' ),
            new RoomClass('Penthouse', null, null, null, 'Available' ),
            new RoomClass('Penthouse', null, null, null, 'Available' ),
            new RoomClass('Penthouse', personArray[1], '2022-07-10', '2022-07-15', 'Reserved' )]
        HotelClass.set(RoomArray,personArray);
        HotelClass.setCanceled(canceledArray);
    }
}
