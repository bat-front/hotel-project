export class CanceledReservation{
    constructor(id,firstName,lastName, dni, firstNamePartner, lastNamePartner, room, checkIn, checkOut, status, price ){
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.DNI = dni;
        this.firstNamePartner = firstNamePartner;
        this.lastNamePartner = lastNamePartner;
        this.room = room;
        this.checkIn = checkIn;
        this.checkOut = checkOut;
        this.status = status;
        this.price = price;
    }
}