export class HotelClass{
    static canceledReservations;
    static rooms;
    static client
    static set(rooms,clients){
        HotelClass.rooms = rooms;
        HotelClass.clients = clients;
    }
    static setCanceled(canceledList){
        HotelClass.canceledReservations = canceledList;
    }
    static addRoom(room){
        HotelClass.rooms.push(room);
    }
    static addClient(client){
        HotelClass.clients.push(client);
    }
    static listAllRooms(){
        return HotelClass.rooms;
    }
    static listAllClients(){
        return HotelClass.clients;
    }
    static listAllCanceledReservations(){
        return HotelClass.canceledReservations;
    }
    static addCanceledReservation(canceledReservation){
        HotelClass.canceledReservations.push(canceledReservation);
    }
}